package com.verizon.samples.SampleProviders;

import android.app.ListActivity;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.widget.SimpleCursorAdapter;

public class Calendar extends ListActivity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        String[] columns = { CalendarContract.Instances.TITLE, CalendarContract.Instances._ID, CalendarContract.Instances.EVENT_ID, CalendarContract.Instances.CALENDAR_COLOR, CalendarContract.Instances.CALENDAR_ID, CalendarContract.Instances.ORGANIZER, CalendarContract.Instances.DESCRIPTION, CalendarContract.Instances.ALL_DAY, CalendarContract.Instances.DTSTART, CalendarContract.Instances.DTEND };
		int tocols[] = new int[] {android.R.id.text1, android.R.id.text2 };
        long begin = System.currentTimeMillis();
		long end = begin + (24 * 60 * 60 * 1000);
		Cursor cursor = CalendarContract.Instances.query(getContentResolver(), columns, begin, end);
		SimpleCursorAdapter sca = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, cursor, columns, tocols);
        setListAdapter(sca);
     }
}
